#include <stdio.h>
#include <string.h>

int main() {
    char cadena1[20] = "Hola";
    char cadena2[10] = " mundo";
    char cadena3[20];
    int longitud;

    // Concatenación de cadenas utilizando la función strcat
    strcat(cadena1, cadena2);

    printf("Cadena concatenada: %s\n", cadena1);

    // Comparación de cadenas utilizando la función strcmp
    if (strcmp(cadena1, "Hola mundo") == 0) {
        printf("Las cadenas son iguales.\n");
    } else {
        printf("Las cadenas son diferentes.\n");
    }

    // Obtención de la longitud de una cadena utilizando la función strlen
    longitud = strlen(cadena1);
    printf("Longitud de la cadena: %d\n", longitud);

    // Copia de una cadena a otra utilizando la función strcpy
    strcpy(cadena3, cadena1);
    printf("Cadena copiada: %s\n", cadena3);

    return 0;
}

