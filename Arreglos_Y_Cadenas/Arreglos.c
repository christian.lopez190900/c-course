#include <stdio.h>

int main() {
    int numeros[5] = {10, 20, 30, 40, 50};
    int suma = 0;
    int i;

    // Suma de elementos en el arreglo
    for (i = 0; i < 5; i++) {
        suma += numeros[i];
    }

    printf("Suma: %d\n", suma);

    return 0;
}

