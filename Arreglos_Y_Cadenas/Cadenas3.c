#include <stdio.h>

int main() {
    char nombre[20];

    printf("Ingrese su nombre: ");
    scanf("%19s", nombre);  // Lee una cadena de hasta 19 caracteres

    printf("Hola, %s!\n", nombre);

    return 0;
}

