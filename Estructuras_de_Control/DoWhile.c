#include <stdio.h>

int main(){
 int contador = 1; /* inicializa el contador */

 do {
    printf( "%d ", contador ); /* despliega el contador */
 } while ( ++contador <= 10 ); /* fin del do...while */
 return 0; 
} 
