#include <stdio.h>

int main() {
    int opcion;

    printf("Ingrese una opción (1-3): ");
    scanf("%d", &opcion);

    switch (opcion) {
        case 1:
            printf("Seleccionaste la opción 1.\n");
            break;
        case 2:
            printf("Seleccionaste la opción 2.\n");
            break;
        case 3:
            printf("Seleccionaste la opción 3.\n");
            break;
        default:
            printf("Opción inválida.\n");
            break;
    }

    return 0;
}

