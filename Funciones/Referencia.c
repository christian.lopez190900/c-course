#include <stdio.h>

void incrementar(int* num);

int main() {
    int numero = 5;
    incrementar(&numero);
    printf("Después de la función: %d\n", numero);
    return 0;
}

void incrementar(int* num) {
    (*num)++;
    printf("Dentro de la función: %d\n", *num);
}
