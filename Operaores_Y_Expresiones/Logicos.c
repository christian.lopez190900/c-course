#include <stdio.h>

int main() {
    int a = 5;
    int b = 10;
    int c = 3;

    if (a < b && b > c) {
        printf("La condición se cumple\n");
    }

    if (a < b || b < c) {
        printf("Al menos una condición se cumple\n");
    }

    if (!(a == b)) {
        printf("La negación de la condición se cumple\n");
    }

    return 0;
}

