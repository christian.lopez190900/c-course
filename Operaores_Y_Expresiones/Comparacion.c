#include <stdio.h>

int main() {
    int a = 5;
    int b = 10;

    if (a == b) {
        printf("a es igual a b\n");
    } else if (a != b) {
        printf("a es diferente de b\n");
    }

    if (a < b) {
        printf("a es menor que b\n");
    }

    if (a > b) {
        printf("a es mayor que b\n");
    }

    if (a <= b) {
        printf("a es menor o igual que b\n");
    }

    if (a >= b) {
        printf("a es mayor o igual que b\n");
    }

    return 0;
}

