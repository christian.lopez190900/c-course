#include <stdio.h>

int main() {
    int a = 5;

    printf("Valor original: %d\n", a);
    printf("Incremento: %d\n", ++a);
    printf("Nuevo valor: %d\n", a);
    
    printf("Decremento: %d\n", --a);
    printf("Nuevo valor: %d\n", a);
    
    return 0;
}

