#include <stdio.h>

int main() {
    int a = 5;  // Representación binaria: 0101
    int b = 3;  // Representación binaria: 0011

    int resultadoAND = a & b;  // Resultado binario: 0001
    int resultadoOR = a | b;   // Resultado binario: 0111
    int resultadoXOR = a ^ b;  // Resultado binario: 0110
    int desplazamientoIzq = a << 1;  // Resultado binario: 1010
    int desplazamientoDer = a >> 1;  // Resultado binario: 0010

    printf("AND: %d\n", resultadoAND);
    printf("OR: %d\n", resultadoOR);
    printf("XOR: %d\n", resultadoXOR);
    printf("Desplazamiento izquierda: %d\n", desplazamientoIzq);
    printf("Desplazamiento derecha: %d\n", desplazamientoDer);

    return 0;
}

