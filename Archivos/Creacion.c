#include <stdio.h>

int main() {
    FILE* archivo = fopen("datos.txt", "w");
    if (archivo == NULL) {
        printf("No se pudo abrir el archivo.\n");
        return 1;
    }

    fprintf(archivo, "¡Hola, mundo!\n");
    fclose(archivo);

    return 0;
}

