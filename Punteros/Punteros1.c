#include <stdio.h>

int main() {
    int num = 5;
    int* ptr = &num;  // Declaración y asignación de un puntero
    
    printf("Valor de num: %d\n", num);
    printf("Dirección de num: %p\n", &num);
    printf("Valor apuntado por ptr: %d\n", *ptr);
    printf("Dirección almacenada en ptr: %p\n", ptr);

    return 0;
}

