#include <stdio.h>

int main() {
    int num = 10;
    int* ptr = &num;

    // Modificación del valor a través del puntero
    *ptr = 20;
    printf("Valor de num: %d\n", num);

    // Puntero a arreglos
    int arr[] = {1, 2, 3, 4, 5};
    int* arrPtr = arr;
    printf("Primer elemento del arreglo: %d\n", *arrPtr);

    // Punteros y asignación dinámica
    int* dynamicPtr = (int*)malloc(sizeof(int));
    *dynamicPtr = 100;
    printf("Valor almacenado en el puntero dinámico: %d\n", *dynamicPtr);
    free(dynamicPtr);

    return 0;
}

