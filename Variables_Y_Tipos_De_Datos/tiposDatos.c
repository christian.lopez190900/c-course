#include <stdio.h>
#include <stdbool.h>

#define Max 10
#define PI 3.141592
int main() {
    int edad = 25;
    float altura = 1.75;
    char genero = 'M';
    printf("Edad: %d\n", edad);
    printf("Altura: %.2f\n", altura);
    printf("Género: %c\n", genero);
    printf("El maximo es : %d\n" , Max);
    printf("El numero Pi es: %.6f\n",PI);
    int *pEdad = &edad;
    printf("Dirección de memoria de edad: %p\n", pEdad);
    printf("Valor de edad a través del puntero: %d\n", *pEdad);

    return 0;
}

